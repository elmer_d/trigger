#! /usr/bin/env node

var path            = require('path');
var fsx             = require('fs-extra');
var triggerPath     = path.normalize(fsx.realpathSync(__dirname));
var config          = fsx.readJsonSync(triggerPath + '/lib/config.json');
var crawler         = require(triggerPath + '/lib/crawler.js');

console.log("\n");

fsx.readdir(process.cwd(), function(err, items) {
    for (var i=0; i<items.length; i++) {
        if (path.extname(items[i]) === '.html') {

            var data = fsx.readFileSync(path.resolve(process.cwd(), items[i]));

            console.log("\x1b[32m" + "**** Getting assets for " + items[i] + " ****\x1b[37m\n");

            crawler.getScripts(data, config);
            crawler.getStyles(data, config);
            crawler.getImages(data, config);
            crawler.getIcons(data, config);

        }
    }
});

process.on('exit', function() {
    console.log("\n");
    console.log("\x1b[32m" + "********************************************\x1b[37m");
    console.log("\x1b[32m" + "*********** Assets Downloaded!!! ***********\x1b[37m");
    console.log("\x1b[32m" + "********************************************\x1b[37m");
    console.log("\n");
});