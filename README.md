# trigger #

Download assets for the html file exported from Variant Builder: Foundry Theme

*****

### Installation ###

1. Clone this repo `$ git clone https://elmer_d@bitbucket.org/elmer_d/trigger.git`

2. Go to the trigger directory `$ cd trigger`

3. Create a tarball `$ npm pack`

4. Install trigger globally using the tarball `$ npm install -g trigger-0.0.1.tgz`

### Usage ###

Go to directory where the exported html is located, then type the trigger command on the command line `$ trigger`

### Local Foundry Folder ###

To get assets from your local foundry directory, go to the trigger config file and change the server path to your local foundry directory


C:/Users/{username}/AppData/Roaming/npm/node_modules/trigger/lib/config.json


```
#!json

{
    "server": "C:/Users/{username}/Documents/foundry/"
}
```