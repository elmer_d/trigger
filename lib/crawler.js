
var path            = require('path');
var fsx             = require('fs-extra');
var libPath         = path.normalize(fsx.realpathSync(__dirname));
var execSync        = require('child_process').execSync;
var lightboxIcons   = require(libPath + '/lightbox-icons.js');
var webURL          = require(libPath + '/regex-weburl.js');
var download        = require(libPath + '/downloader.js');
var iconList        = require(libPath + '/icons.js');
var cheerio         = require('cheerio');

module.exports = {

    getScripts: function(data, config) {

        $ = cheerio.load(data, { decodeEntities: false });

        var js = $('script');

        if (js.length > 0) {

            // Create js folder if it doesn't exist
            if (!fsx.existsSync('js')) {
                execSync('mkdir js');
            }

            // Get all javascript used
            js.each(function() {
                // If the file is located within the server
                if (!webURL.regEx.exec($(this).attr('src'))) {

                    // The script file path
                    var script = $(this).attr('src');

                    // Get the scripts
                    download(config.server + script, script);

                    // If lightbox is used
                    if (script === 'js/lightbox.min.js') {

                        // Create lightbox directory
                        if (!fsx.existsSync('img\\lightbox')) {
                            execSync('mkdir img\\lightbox');
                        }

                        // Get lightbox assets
                        lightboxIcons.forEach(function(icon) {
                            download(config.server + 'img/lightbox/' + icon, 'img/lightbox/' + icon);
                        });
                    }
                }
            });
        }
    },
    getStyles: function(data, config) {

        $ = cheerio.load(data, { decodeEntities: false });

        var css = $('link');

        if (css.length > 0) {

            // Create css filder if it doesn't exist
            if (!fsx.existsSync('css')) {
                execSync('mkdir css');
            }

            // Get all styles used
            css.each(function() {

                // If the file is located within the server
                if (!webURL.regEx.exec($(this).attr('href'))) {

                    // The css file path
                    var style = $(this).attr('href');

                    // Get the css
                    download(config.server + style, style);
                }
            });
        }
    },
    getImages: function(data, config) {

        $ = cheerio.load(data, { decodeEntities: false });

        var img = $('img');

        if (img.length > 0) {

            // Create img folder if it doesn't exist
            if (!fsx.existsSync('img')) {
                execSync('mkdir img');
            }

            // Get all images used
            img.each(function() {

                // If the file is located within the server
                if (!webURL.regEx.exec($(this).attr('src'))) {

                    // The image file path
                    var image = $(this).attr('src');

                    // Get the images
                    download(config.server + image, image);
                }
            });
        }
    },
    getIcons: function(data, config) {
        // Select element that uses icons 
        var icons = {
            themify: $('[class^=ti-]'),
            glyphicons: $('[class^=glyphicons-]'),
            peicon7: $('[class^=pe-7s-]'),
            etline: $('[class*=icon-]'),
            fontawesome: $('[class*=fa-]')
        };

        // Check for classes that uses themify icon as css content 
        var hasThemify = function() {
            if ($('.has-dropdown').length > 0 || $('.accordion-2').length > 0) {
                return true;
            } else {
                return false;
            }
        }

        // Get all icon fonts used
        for (var icon in icons) {

            // If current icon font is used
            if (icons[icon].length > 0) {

                // Create fonts folder if it doesn't exist
                if (!fsx.existsSync('fonts')) {
                    execSync('mkdir fonts');
                }

                // Get the icon fonts
                iconList[icon].forEach(function(item) {
                    download(config.server + 'fonts/' + item, 'fonts/' + item);
                });

            } else {

                // If themify class prefix is not present but..
                if (icon === 'themify') {

                    // If themify is used as css content
                    if (hasThemify) {

                        // Create fonts folder if it doesn't exist
                        if (!fsx.existsSync('fonts')) {
                            execSync('mkdir fonts');
                        }

                        // Get the themify fonts
                        iconList['themify'].forEach(function(item) {
                            download(config.server + 'fonts/' + item, 'fonts/' + item);
                        });
                    }
                }
            }
        }
    }
}
