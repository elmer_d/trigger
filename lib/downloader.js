var fsx = require('fs-extra');
var math = require('mathjs');
var request = require('request');
var progress = require('request-progress');
var execSync = require('child_process').execSync;

var webURL = require('./regex-weburl.js');

// Round off the data chunks being transferred
function transferred(state) {
    return math.round((state.size.transferred / state.size.total) * 100) + '%';
}

// Downloader
module.exports = function(sourcePath, filePath) {

    // if sourcePath is a remote server
    if (webURL.regEx.exec(sourcePath)) {

        var req = request(sourcePath);
        var isFound = false;

        progress(req).on('error', function(error) {

            // Display errors
            if (!error.code === 'ETIMEDOUT') console.log("ERROR", error);                   // TODO: determine what to do on server TIMEOUT

        }).on('progress', function(state) {

            // Display the precentage of the file being downloaded
            console.log(filePath, "\x1b[35m" + transferred(state) + "\x1b[37m");

        }).on('response', function(response) {

            // Display the status code
            console.log(filePath + " \x1b[36mSTATUS CODE\x1b[37m", "\x1b[33m" + response.statusCode + "\x1b[37m");

            // If status code is 404
            if (response.statusCode === 404) {

                // Display FILE NOT FOUND!
                console.log("\x1b[31m" + filePath + " FILE NOT FOUND!\x1b[37m");

            } else {

                // If status is not 404 file is found
                isFound = true;

                // Write the file
                req.pipe(fsx.createWriteStream(filePath))
            }

        }).on('end', function() {

            // If status is not 404 and file is done writing, display DONE
            if (isFound) {
                console.log("\x1b[32m" + filePath + " DONE\x1b[37m");
            }

        });

    } else {

        // if sourcePath is local 
        var readStream = fsx.createReadStream(sourcePath);

        readStream.on('open', function() {
            readStream.pipe(fsx.createWriteStream(filePath));
            console.log(filePath + "\x1b[32m" + " DONE\x1b[37m");
        });

        readStream.on('error', function(err) {
            console.log("\x1b[31m" + err + " \x1b[37m");
        });
    }

};
