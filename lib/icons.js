module.exports = {
    "etline" : [
        "et-line.eot",
        "et-line.svg",
        "et-line.ttf",
        "et-line.woff"
    ],
    "fontawesome" : [
        "FontAwesome.otf",
        "fontawesome-webfont.eot",
        "fontawesome-webfont.svg",
        "fontawesome-webfont.ttf",
        "fontawesome-webfont.woff",
        "fontawesome-webfont.woff2"
    ],
    "glyphicons" : [
        "glyphicons-halflings-regular.eot",
        "glyphicons-halflings-regular.svg",
        "glyphicons-halflings-regular.ttf",
        "glyphicons-halflings-regular.woff"
    ],
    "peicon7" : [
        "Pe-icon-7-stroke.eot",
        "Pe-icon-7-stroke.svg",
        "Pe-icon-7-stroke.ttf",
        "Pe-icon-7-stroke.woff"
    ],
    "themify" : [
        "themify.eot",
        "themify.svg",
        "themify.ttf",
        "themify.woff"
    ]
};